#!/usr/bin/python3
import random

class Deck(object):
    def __init__(self, shuffled: bool = True):
        self.cardTypes = ["Heart", "Diamonds", "Spades", "Clubs"]
        self.cardNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        self._deck = self.buildDeck()
        self._validCards = set(self._deck)
        self._discard = list()
        if shuffled == True:
            self.shuffleDeck()

    @property
    def deck(self):
        return self._deck

    @deck.setter
    def deck(self, card):
        if card in self._validCards:
            self._deck.append(card)
        else:
            raise ValueError

    @deck.deleter
    def deck(self):
        del self._deck



    def buildDeck(self):
        deck = list()
        for i in self.cardTypes:
            for n in self.cardNumbers:
                deck.append((n, i))
        return deck

    def shuffleDeck(self, shuffleTimes=3):
        """
            Shuffle the deck

            Args: 
                ShuffleTimes: Times the deck will be shuffled
                Defaults to 3 shuffles
        """
        for _ in range(0, shuffleTimes):
            random.shuffle(self.deck)
        return self.deck
        
    def sortDeck(self):
        raise NotImplementedError

    def chooseRandomeCard(self):
    """
        returns a randome card,
        Will not take the card away from the deck
    """
    return random.choice(self.deck)

    def discardCard(self, *card):
        raise NotImplementedError