#!/usr/bin/env python3

class Player():
    """
        Player Object
    """
    def __init__(self, name):
        self.name = name
        self.hand = []
        self.tokens = 0

    def drawCard(self, card):
            self.hand.append(card)

    def discardCard(self, card):
        """
            Discard a card from the hand.
            If card is in hand, retrun index of card.
            Throws ValueError except if not it in hand.

            :Param card:
                Card to be discarded from the hand
        """
        try:
            return self.hand.index(card)
        except ValueError:
            return False

    def getCards(self):
        return self.hand

    def getToken(self):
        return self.tokens

    def addTokenToTable(self, token):
        if token <= self.tokens:
            self.tokens -= token
            return True
        else:
            return False

    def TABLEFLIP(self):
        """
            A player flips the table
        """
        return "(ノಠ益ಠ)ノ彡┻━┻" 