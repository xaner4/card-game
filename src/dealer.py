#!/usr/bin/env python3

from cards import Cards
from player import Player

deck = Cards(shuffle=True)

Player().discardCard()

class Dealer:
    def __init__(self):
        self.tokensPot = 0
        self.players = []
        self.discard = Cards.discardCard
        self.playersCardsOnTable = {}
    
    def dealCards(self):
        pass

    def dealCard(self):
        pass

    def getToken(self):
        pass

    def dealToken(self):
        pass

    